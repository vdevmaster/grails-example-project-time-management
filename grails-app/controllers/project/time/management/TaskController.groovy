package project.time.management

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured


@Secured(["ROLE_USER"])
@Transactional(readOnly = true)
class TaskController extends BaseController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]


    def index(Long projectId, Long stepId,Integer max) {

        params.max = Math.min(max ?: 10, 100)

        def project = getProjectWithIdForCurrentUser(projectId)
        if (notHaveProject(project)) {
            renderNotFound("Project not found")
        }

        def step = getStepWithIdForProject(stepId, project)
        if (notHaveStep(step)) {
            renderNotFound("Step not found")
        }


        String order = "asc"

        if (params.order) {
            String o = params.order
            if (o == "desc") {
                order = "desc"
            }
        }

        String sortField = "dateCreated"
        if (params.sort) {
            def s = params.sort
            switch (s) {
                case "title":
                    sortField = "title"
                    break
                case "priority":
                    sortField = "priority.number"
                    break

                case "targetTime":
                    sortField = "targetTime"
                    break
                case "totalTaskTime":
                    sortField = ""
                    break
                case "status":
                    sortField = "taskStatus.title"
                    break
                case "lastUpdated":
                    sortField = "lastUpdated"
                    break
            }
        }

        def tasks


        if (sortField == "") {
            tasks = Task.findAllByProjectAndStep(project, step,[max:params.max,offset:params.offset])
            tasks = tasks.sort { it.getTotalTaskTime()  }
            if (order == "desc") {
                tasks = tasks.reverse()
            }

        } else {
            String sortBy = " order by t.$sortField $order"
            tasks = Task.findAll("from Task t where t.project=? and t.step=? " + sortBy, [project, step], params)

        }


        def allTaskForProjectAndStep = Task.findAllByProjectAndStep(project, step)

        def size = allTaskForProjectAndStep == null ? 0 : allTaskForProjectAndStep.size()

        render view: "index", model: [taskInstanceList: tasks, taskInstanceCount: size, projectInstance: project, stepInstance: step]
    }

    def show(Long projectId, Long stepId, Long taskId) {

        def taskInstance = Task.get(taskId)

        if (taskInstance == null) {
            render "Bug Task instance is null"
            return
        }

        def user = getUser()

        def project = Project.findByUserAndId(user, projectId)
        if (project == null) {
            render "Bug project for task instance is null"
            return
        }

        def step = Step.findByProjectAndId(project, stepId)

        if (step == null) {
            render "Bug step for task instance is null"
            return
        }

        taskInstance.project = project
        taskInstance.step = step


        render view: "show", model: [taskInstance: taskInstance]
    }

    def create(Long projectId, Long stepId) {

        def project = getProjectWithIdForCurrentUser(projectId)

        if (notHaveProject(project)) {
            renderNotFound("Project for task not found")
        }

        def step = getStepWithIdForProject(stepId, project)

        if (notHaveStep(step)) {
            renderNotFound("Step for task not found")
        }


        def task = new Task()

        task.project = project
        task.step = step

        render view: "create", model: [taskInstance: task]
    }

    @Transactional
    def save(Task taskInstance) {


        if (taskInstance == null) {
            notFound()
            return
        }

        def projectId = params.projectId

        def project = getProjectWithIdForCurrentUser(projectId)


        if (notHaveProject(project)) {
            renderNotFound("Project for task not found")
        }

        def step = getStepWithIdForProject(params.stepId, project)

        if (notHaveStep(step)) {
            renderNotFound("Step for task not found")
        }

        taskInstance.step = step
        taskInstance.project = project

        taskInstance = setTimeFromInput(taskInstance)

        if (taskInstance.hasErrors()) {
            respond taskInstance.errors, view: 'create'
            return
        }


        taskInstance = setPriorityFromInput(taskInstance)

        taskInstance = setTaskTypeFromInput(taskInstance)

        taskInstance = setTaskStatusFromInput(taskInstance)

        taskInstance.save()

        flash.message = message(code: 'default.created.message', args: [message(code: 'task.label', default: 'Task'), taskInstance.id])

        redirect action: "show", params: [projectId: taskInstance.project.id, stepId: taskInstance.step.id, taskId: taskInstance.id]


    }

    private Task setTaskStatusFromInput(Task task) {

        def taskStatusNumber = params.taskStatusNumber

        def taskStatus = TaskStatus.findByNumber(taskStatusNumber)

        task.taskStatus = taskStatus

        return task
    }


    private Task setTaskTypeFromInput(Task task) {

        def taskTypeTitle = params.taskTypeTitle

        def taskType = TaskType.findByTitle(taskTypeTitle)

        if (taskType == null) {
            render "Bug task type for $task.title is null"
            return
        }

        task.taskType = taskType

        return task
    }

    private Task setPriorityFromInput(Task task) {

        def priorityNumber = params.priorityNumber

        def priority = Priority.findByNumber(priorityNumber)

        if (priority == null) {
            render "Bug priority is null"
            return
        }

        task.priority = priority

        return task
    }

    def edit(Long projectId, Long stepId, Long taskId) {


        def taskInstance = Task.get(taskId)

        if (taskInstance == null) {
            render "Bug task instance is null"
            return
        }


        def user = getUser()
        def project = Project.findByUserAndId(user, projectId)

        if (project == null) {
            notFound()
            return
        }

        def step = Step.findByProjectAndId(project, stepId)

        if (step == null) {
            notFound()
            return
        }

        taskInstance.project = project
        taskInstance.step = step

        render view: "edit", model: [taskInstance: taskInstance]
    }


    private Task setTimeFromInput(Task task) {
        Long hours = 0
        Long minutes = 0

        try {
            def pHours = params.targetTimeHours
            if (pHours != null && pHours != "") {
                hours = Long.parseLong(pHours)
            }
        } catch (Exception e) {
            render "Parse hours exception"
            return
        }

        try {
            def pMinutes = params.targetTimeMinutes
            if (pMinutes != null && pMinutes != "") {
                minutes = Long.parseLong(pMinutes)
            }
        } catch (Exception e) {
            render "Parse minutes exception"
            return
        }

        task.setTargetTimeFromHoursAndMiutes(hours, minutes)

        return task
    }

    @Transactional
    def update(Task taskInstance) {
        if (taskInstance == null) {
            notFound()
            return
        }

        def project = getProjectWithIdForCurrentUser(taskInstance.project.id)

        if (notHaveProject(project)) {
            render "SECURITY OTHER PROJECT"
            return
        }

        def step = getStepWithIdForProject(params.stepId, project)

        if (notHaveStep(step)) {
            renderNotFound("Step for task not found")
        }

        taskInstance.project = project
        taskInstance.step = step

        taskInstance = setTimeFromInput(taskInstance)

        if (taskInstance.hasErrors()) {
            respond taskInstance.errors, view: 'edit'
            return
        }

        taskInstance = setPriorityFromInput(taskInstance)

        taskInstance = setTaskTypeFromInput(taskInstance)

        taskInstance = setTaskStatusFromInput(taskInstance)

        taskInstance.save flush: true


        flash.message = message(code: 'default.updated.message', args: [message(code: 'Task.label', default: 'Task'), taskInstance.id])

        redirect method: "GET", action: "show", params: [projectId: project.id, stepId: step.id, taskId: taskInstance.id]


    }

    @Transactional
    def delete(Task taskInstance) {

        if (taskInstance == null) {
            notFound()
            return
        }

        def project = getProjectWithIdForCurrentUser(taskInstance.project.id)

        if (project.id != taskInstance.project.id) {
            securityAccessDeniedExceptionProjectNotBelongsToUser("for task $taskInstance.title")
        }

        def step = getStepWithIdForProject(taskInstance.step.id, project)

        if (notHaveStep(step)) {
            renderNotFound("Step for task not found")
        }

        taskInstance.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'task.label', default: 'Task'), taskInstance.id])

        redirect method: "GET", action: "index", params: [projectId: project.id, stepId: step.id]


    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'task.label', default: 'Task'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
