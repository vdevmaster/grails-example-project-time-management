package project.time.management

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(["ROLE_USER"])
@Transactional(readOnly = true)
class ProjectController extends BaseController {


    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]


    def index(Integer max) {

        params.max = Math.min(max ?: 10, 100)

        boolean active = true, frozen = false, completed = false

        String typeProject = "onlyActive"
        if (params.onlyActive) {
            active = true
            typeProject = "onlyActive"
        } else if (params.onlyFrozen) {
            active = false
            frozen = true
            typeProject = "onlyFrozen"
        } else if (params.onlyCompleted) {
            active = false
            completed = true
            typeProject = "onlyCompleted"
        }

        def projects = null

        def user = getUser()



        String order = "asc"

        if (params.order) {
            String o = params.order
            if (o == "desc") {
                order = "desc"
            }
        }

        String sortField = "title"
        if (params.sort) {
            def s = params.sort
            switch (s) {
                case "title":
                    sortField = "title"
                    break
                case "target":
                    sortField = "target"
                    break
                case "body":
                    sortField = "body"
                    break
                case "startDate":
                    sortField = "startDate"
                    break
                case "endDate":
                    sortField = "startDate"
                    break
                case "lastUpdated":
                    sortField="lastUpdated"
                    break
            }
        }

        String sortBy = " order by p.$sortField $order"

        def totalProjects
        if (active) {
            projects = Project.findAll("from Project p where p.user=? and p.status.statusType.title =?" + sortBy, [user, StatusType.ACTIVE], params)
            totalProjects = Project.findAll("from Project p where p.user=? and p.status.statusType.title =?",[user,StatusType.ACTIVE])
        } else if (frozen) {
            projects = Project.findAll("from Project p where p.user=? and p.status.statusType.title =?" + sortBy, [user, StatusType.FROZEN], params)
            totalProjects = Project.findAll("from Project p where p.user=? and p.status.statusType.title =?",[user,StatusType.FROZEN])
        } else if (completed) {
            projects = Project.findAll("from Project p where p.user=? and p.status.statusType.title =?" + sortBy, [user, StatusType.COMPLETE], params)
            totalProjects = Project.findAll("from Project p where p.user=? and p.status.statusType.title =?",[user,StatusType.COMPLETE])
        } else {
            render "Bug: not from project type "
            return
        }



        def size = totalProjects == null ? 0 : totalProjects.size()

        render view: 'index', model: [projectInstanceList: projects, projectInstanceCount: size, typeProject: typeProject]
    }

    def show(Long projectId) {

        def user = getUser()
        def projectInstance = Project.findByUserAndId(user, projectId)

        if (projectInstance == null) {
            render "Bug project is null"
            return
        }

        def steps = projectInstance.steps

        if (steps != null && steps.size() > 1) {
            projectInstance.steps = projectInstance.steps.sort { it.orderNumber }
        }

        def displayCreateButton = isDisplayCreateButton((Status)projectInstance.status)

        render view: "show", model: [projectInstance:projectInstance,displayCreateButton:displayCreateButton]
    }

    def create() {
        respond new Project(params)
    }

    @Transactional
    def save(Project projectInstance) {


        if (projectInstance == null) {
            notFound()
            return
        }


        def user = getUser()

        projectInstance.user = user


        if (projectInstance.hasErrors()) {
            respond projectInstance.errors, view: 'create'
            return
        }

        projectInstance.save()

        def statusTypeTitle = params.statusTypeTitle

        Status.createSaveAndUpdateProjectStatus(projectInstance, statusTypeTitle)

        flash.message = message(code: 'default.created.message', args: [message(code: 'project.label', default: 'Проект'), projectInstance.id])

        redirect action: "show", params: [projectId: projectInstance.id]


    }

    def edit(Project projectInstance) {

        respond projectInstance
    }

    @Transactional
    def update(Project projectInstance) {


        if (projectInstance == null) {
            notFound()
            return
        }

        def user = getUser()


        if (projectInstance.hasErrors()) {
            respond projectInstance.errors, view: 'edit'
            return
        }

        projectInstance.save flush: true

        Status.updateProjectStatus(projectInstance, params.statusTypeTitle)

        flash.message = message(code: 'default.updated.message', args: [message(code: 'project.label', default: 'project'), projectInstance.id])


        redirect action: "show", params: [projectId: projectInstance.id]


    }

    @Transactional
    def delete(Project projectInstance) {


        if (projectInstance == null) {
            notFound()
            return
        }


        def projectForUser = getProjectWithIdForCurrentUser(projectInstance.id)

        if (projectInstance.id != projectForUser.id) {
            render "Security: Try delete project who not belong this user"
            return
        }

        projectInstance.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'project.label', default: 'project'), projectInstance.id])

        redirect method: "GET", action: "index", params: [onlyActive: true]

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'project.label', default: 'Project'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
