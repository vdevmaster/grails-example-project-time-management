package project.time.management



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TaskStatusController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {

        params.max = Math.min(max ?: 10, 100)
        respond TaskStatus.list(params), model:[taskStatusInstanceCount: TaskStatus.count()]
    }

    def show(TaskStatus taskStatusInstance) {
        respond taskStatusInstance
    }

    def create() {
        respond new TaskStatus(params)
    }

    @Transactional
    def save(TaskStatus taskStatusInstance) {
        if (taskStatusInstance == null) {
            notFound()
            return
        }

        if (taskStatusInstance.hasErrors()) {
            respond taskStatusInstance.errors, view:'create'
            return
        }

        taskStatusInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'taskStatus.label', default: 'TaskStatus'), taskStatusInstance.id])
                redirect taskStatusInstance
            }
            '*' { respond taskStatusInstance, [status: CREATED] }
        }
    }

    def edit(TaskStatus taskStatusInstance) {
        respond taskStatusInstance
    }

    @Transactional
    def update(TaskStatus taskStatusInstance) {
        if (taskStatusInstance == null) {
            notFound()
            return
        }

        if (taskStatusInstance.hasErrors()) {
            respond taskStatusInstance.errors, view:'edit'
            return
        }

        taskStatusInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TaskStatus.label', default: 'TaskStatus'), taskStatusInstance.id])
                redirect taskStatusInstance
            }
            '*'{ respond taskStatusInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TaskStatus taskStatusInstance) {

        if (taskStatusInstance == null) {
            notFound()
            return
        }

        taskStatusInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TaskStatus.label', default: 'TaskStatus'), taskStatusInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'taskStatus.label', default: 'TaskStatus'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
