package project.time.management


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured


@Secured(["ROLE_USER"])
@Transactional(readOnly = true)
class StepController extends BaseController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]



    def index(Long projectId,Integer max) {

        params.max = Math.min(max ?: 10, 100)

        def user = getUser()

        def project = Project.findByUserAndId(user, projectId)


        if (project == null) {
            render "Bug : Project is null index method"
            return
        }


        if (project.steps != null && project.steps.size() > 1) {
            project.steps = project.steps.sort{it.orderNumber}
        }


        def steps =  Step.findAllByProject(project,params)

        project.steps = steps

        def totalProjects = Step.findAllByProject(project)

        def size = totalProjects == null?0:totalProjects.size()

        render view: "index",model: [projectInstance:project,stepInstanceCount:size]

    }

    def show(Long projectId,Long stepId) {

        def user = getUser()

        def project = Project.findByUserAndId(user, projectId)


        if (project == null) {
            render "Bug : Project is null"
            return
        }

        def stepInstance = Step.findByProjectAndId(project,stepId)

        if (stepInstance == null) {
            render "Bug : Step is null"
            return
        }

        stepInstance.project = project

        def steps = project.steps

        def displayCreateButton = isDisplayCreateButton((Status)stepInstance.status)

        render view: "show", model: [stepInstance: stepInstance,steps:steps,displayCreateButton:displayCreateButton]
    }

    def create(Long projectId) {


        def step = new Step(params)

        step.orderNumber = 1


        def user = springSecurityService.currentUser


        def project = Project.findByUserAndId(user, projectId)


        if (project == null) {
            notFound()
            return
        }


        step.project = project

        def steps = project.steps


        if (steps != null && steps.size() > 0) {
            step.orderNumber = steps.size() + 1
        }

        render view: "create", model: [stepInstance: step, projectId: projectId]
    }

    @Transactional
    def save(Step stepInstance) {


        if (stepInstance == null) {
            notFound()
            return
        }

        stepInstance = new Step(params)


        def projectId = params.projectId

        def user = getUser()
        def project = Project.findByUserAndId(user, projectId)


        if (project == null) {
            render "Bug project is null for step"
            return
        }


        stepInstance.project = project


        if (stepInstance.hasErrors()) {
            respond stepInstance.errors, view: 'create'
            return
        }

        stepInstance.save flush: true

        Status.createSaveAndUpdateStepStatus(stepInstance, params.statusTypeTitle)


        flash.message = message(code: 'default.created.message', args: [message(code: 'step.label', default: 'Step'), stepInstance.id])

        redirect action: "show", params:[projectId: stepInstance.project.id,stepId:stepInstance.id]


    }

    def edit(Step stepInstance) {
        respond stepInstance
    }

    @Transactional
    def update(Step stepInstance) {


        if (stepInstance == null) {
            notFound()
            return
        }

        if (stepInstance.hasErrors()) {
            respond stepInstance.errors, view: 'edit'
            return
        }


        def projectId = params.projectId

        def user = getUser()
        def project = Project.findByUserAndId(user, projectId)


        if (project == null) {
            render "Bug project is null for step"
            return
        }


        stepInstance.project = project

        stepInstance.save flush: true

        Status.updateStepStatus(stepInstance, params.statusTypeTitle)


        flash.message = message(code: 'default.updated.message', args: [message(code: 'step.label', default: 'Step'), stepInstance.id])

        redirect action: "show" , params:[projectId:stepInstance.project.id,stepId: stepInstance.id]


    }

    @Transactional
    def delete(Step stepInstance) {

        if (stepInstance == null) {
            notFound()
            return
        }

        def project = getProjectWithIdForCurrentUser(stepInstance.project.id)

        if (notHaveProject(project)) {
            renderNotFound("Project is null for current user")
        }


        stepInstance.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'step.label', default: 'Step'), stepInstance.id])

        redirect method: "GET", action: "index",params: [projectId:project.id]


    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'step.label', default: 'Step'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
