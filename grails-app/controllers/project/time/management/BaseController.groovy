package project.time.management

class BaseController {

    def springSecurityService

    User getUser() {
        return springSecurityService.currentUser
    }

    Project getProjectWithIdForCurrentUser(def projectId) {
        return Project.findByUserAndId(getUser(), projectId)
    }

    boolean hasProject(Project project) {
        if (project == null) {
           return false
        }
        return true
    }

    boolean notHaveProject(Project project) {
        return !hasProject(project)
    }

    def renderNotFound(String message) {
        render message
        return
    }

    Step getStepWithIdForProject( def stepId,Project project) {
        return Step.findByProjectAndId(project, stepId)
    }

    boolean hasStep(Step step) {
        if (step == null) {
           return false
        }
        return true
    }

    boolean notHaveStep(Step step) {
        return !hasStep(step)
    }

    Task getTaskWithIdForStep( def stepId,Step step) {
        return Task.findByStepAndId(step, stepId)
    }

    boolean hasTask(Task task) {
        if (task == null) {
            return false
        }
        return true
    }

    boolean notHaveTask(Task task) {
        return !hasTask(task)
    }


    boolean securityProjectNotBelongsForCurrentUser(Project project) {
        return !securityIsProjectForCurrentUser(project)
    }


    boolean securityIsProjectForCurrentUser(Project project) {
        if (project.user.id == getUser().id) {
            return true
        }
        return false
    }

    boolean isStepForProject(Project project, Step step) {
        if (step.project.id == project.id) {
            return true
        }
        return false
    }

    def accessDenied(String message) {
        render ""+message
        return
    }

    def securityAccessDeniedExceptionProjectNotBelongsToUser(String message) {
        return securityAccessDenied(" PROJECT NOT BELONGS CURRENT USER: "+getUser().username+" "+message)
    }

    def securityAccessDenied(String message) {
        render "SECURITY ACCESS DENIED: "+message
        return

    }


    Boolean isDisplayCreateButton(Status status) {

        if (status.statusType.title == StatusType.ACTIVE) {
            return  true
        }
        return false
    }

}
