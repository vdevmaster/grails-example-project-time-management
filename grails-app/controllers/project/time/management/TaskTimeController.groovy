package project.time.management


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured


@Secured(["ROLE_USER"])
@Transactional(readOnly = true)
class TaskTimeController extends BaseController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long projectId, Long stepId, Long taskId, Integer max) {

        params.max = Math.min(max ?: 10, 100)


        def project = getProjectWithIdForCurrentUser(projectId)

        if (notHaveProject(project)) {
            renderNotFound("Project not found")
        }

        def step = getStepWithIdForProject(stepId, project)
        if (notHaveStep(step)) {
            renderNotFound("Step not found ")
        }

        def task = getTaskWithIdForStep(taskId, step)
        if (notHaveTask(task)) {
            renderNotFound("Task not found")
        }

        task.step = step
        task.project = project

        def taskTimes = TaskTime.findAllByTask(task, params)
        def taskTimesTotal = TaskTime.findAllByTask(task)

        def size = taskTimesTotal == null ? 0 : taskTimesTotal.size()

        render view: "index", model: [taskInstance: task, taskTimeInstanceList: taskTimes, taskTimeInstanceCount: size]
    }

    def show(Long projectId, Long stepId, Long taskId, Long taskTimeId) {

        def project = getProjectWithIdForCurrentUser(projectId)

        if (notHaveProject(project)) {
            renderNotFound("Not found project")
        }

        def step = getStepWithIdForProject(stepId, project)

        if (notHaveStep(step)) {
            renderNotFound("Not found step")
        }

        def task = getTaskWithIdForStep(taskId, step)

        if (notHaveTask(task)) {
            renderNotFound("Not found task")
        }

        def taskTime = TaskTime.findByTaskAndId(task, taskTimeId)

        render view: "show", model: [taskTimeInstance: taskTime]
    }

    def create(Long projectId, Long stepId, Long taskId) {


        def project = getProjectWithIdForCurrentUser(projectId)

        if (notHaveProject(project)) {
            renderNotFound("Not found project")
        }

        def step = getStepWithIdForProject(stepId, project)

        if (notHaveStep(step)) {
            renderNotFound("Not found step")
        }

        def task = getTaskWithIdForStep(taskId, step)

        if (notHaveTask(task)) {
            renderNotFound("Not found task")
        }

        TaskTime taskTime = new TaskTime()
        step.project = project
        task.step = step
        taskTime.task = task

        render view: "create", model: [taskTimeInstance: taskTime]
    }

    @Transactional
    def save(TaskTime taskTimeInstance) {
        if (taskTimeInstance == null) {
            notFound()
            return
        }


        def project = getProjectWithIdForCurrentUser(params.projectId)

        if (notHaveProject(project)) {
            renderNotFound("Not found project")
        }

        def step = getStepWithIdForProject(params.stepId, project)

        if (notHaveStep(step)) {
            renderNotFound("Not found step")
        }

        def task = getTaskWithIdForStep(params.taskId, step)

        if (notHaveTask(task)) {
            renderNotFound("Not found task")
        }


        def timeHours = params.timeHours
        def timeMinutes = params.timeMinutes

        Long hours = 0
        try {
            hours = Long.parseLong(timeHours)
        } catch (Exception e) {
            render "Parse hours exception hours to long"

            return
        }
        Long minutes = 0
        try {
            minutes = Long.parseLong(timeMinutes)
        } catch (Exception e) {
            render "Parse minutes exception minutes to long"
            return
        }


        taskTimeInstance.setTimeFromHoursAndMinutes(hours, minutes)


        if (taskTimeInstance.hasErrors()) {
            respond taskTimeInstance.errors, view: 'create'
            return
        }

        taskTimeInstance.task = task

        taskTimeInstance.save flush: true


        flash.message = message(code: 'default.created.message', args: [message(code: 'taskTime.label', default: 'TaskTime'), taskTimeInstance.id])


        redirect action: "show", params: [projectId: project.id, stepId: step.id, taskId: task.id, taskTimeId: taskTimeInstance.id]


    }

    def edit(Long projectId, Long stepId, Long taskId, Long taskTimeId) {

        def project = getProjectWithIdForCurrentUser(projectId)

        if (notHaveProject(project)) {
            renderNotFound("Not found project")
        }

        def step = getStepWithIdForProject(stepId, project)

        if (notHaveStep(step)) {
            renderNotFound("Not found step")
        }

        def task = getTaskWithIdForStep(taskId, step)

        if (notHaveTask(task)) {
            renderNotFound("Not found task")
        }

        step.project = project
        task.step = step

        def taskTime = TaskTime.findByTaskAndId(task, taskTimeId)
        taskTime.task = task


        render view: "edit", model: [taskTimeInstance: taskTime]
    }

    @Transactional
    def update(TaskTime taskTimeInstance) {
        if (taskTimeInstance == null) {
            notFound()
            return
        }

        def project = getProjectWithIdForCurrentUser(params.projectId)

        if (notHaveProject(project)) {
            renderNotFound("Not found project")
        }

        def step = getStepWithIdForProject(params.stepId, project)

        if (notHaveStep(step)) {
            renderNotFound("Not found step")
        }

        def task = getTaskWithIdForStep(params.taskId, step)

        if (notHaveTask(task)) {
            renderNotFound("Not found task")
        }

        def timeHours = params.timeHours
        def timeMinutes = params.timeMinutes

        Long hours = 0
        try {
            hours = Long.parseLong(timeHours)
        } catch (Exception e) {
            render "Parse hours exception hours to long"

            return
        }
        Long minutes = 0
        try {
            minutes = Long.parseLong(timeMinutes)
        } catch (Exception e) {
            render "Parse minutes exception minutes to long"
            return
        }


        taskTimeInstance.setTimeFromHoursAndMinutes(hours, minutes)


        if (taskTimeInstance.hasErrors()) {
            respond taskTimeInstance.errors, view: 'edit'
            return
        }

        taskTimeInstance.save flush: true

        flash.message = message(code: 'default.updated.message', args: [message(code: 'TaskTime.label', default: 'TaskTime'), taskTimeInstance.id])

        redirect action: "show", params: [projectId: project.id, stepId: step.id, taskId: task.id, taskTimeId: taskTimeInstance.id]


    }

    @Transactional
    def delete(TaskTime taskTimeInstance) {

        if (taskTimeInstance == null) {
            notFound()
            return
        }

        def project = getProjectWithIdForCurrentUser(taskTimeInstance.task.project.id)

        if (notHaveProject(project)) {
            renderNotFound("Not found project")
        }

        if (securityProjectNotBelongsForCurrentUser(project)) {
            securityAccessDeniedExceptionProjectNotBelongsToUser(" in taskTime $taskTimeInstance.id")
        }


        def step = getStepWithIdForProject(taskTimeInstance.task.step.id, project)

        if (notHaveStep(step)) {
            renderNotFound("Not found step")
        }

        def task = getTaskWithIdForStep(taskTimeInstance.task.id, step)

        if (notHaveTask(task)) {
            renderNotFound("Not found task")
        }

        taskTimeInstance.delete flush: true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'taskTime.label', default: 'TaskTime'), taskTimeInstance.id])

        redirect method: "GET", action: "index", params: [projectId: project.id, stepId: step.id, taskId: task.id]


    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'taskTime.label', default: 'TaskTime'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
