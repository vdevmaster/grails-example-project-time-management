package project.time.management



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(["ROLE_ADMIN"])
@Transactional(readOnly = true)
class CommentTypeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond CommentType.list(params), model:[commentTypeInstanceCount: CommentType.count()]
    }

    def show(CommentType commentTypeInstance) {
        respond commentTypeInstance
    }

    def create() {
        respond new CommentType(params)
    }

    @Transactional
    def save(CommentType commentTypeInstance) {
        if (commentTypeInstance == null) {
            notFound()
            return
        }

        if (commentTypeInstance.hasErrors()) {
            respond commentTypeInstance.errors, view:'create'
            return
        }

        commentTypeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'commentType.label', default: 'CommentType'), commentTypeInstance.id])
                redirect commentTypeInstance
            }
            '*' { respond commentTypeInstance, [status: CREATED] }
        }
    }

    def edit(CommentType commentTypeInstance) {
        respond commentTypeInstance
    }

    @Transactional
    def update(CommentType commentTypeInstance) {
        if (commentTypeInstance == null) {
            notFound()
            return
        }

        if (commentTypeInstance.hasErrors()) {
            respond commentTypeInstance.errors, view:'edit'
            return
        }

        commentTypeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'CommentType.label', default: 'CommentType'), commentTypeInstance.id])
                redirect commentTypeInstance
            }
            '*'{ respond commentTypeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(CommentType commentTypeInstance) {

        if (commentTypeInstance == null) {
            notFound()
            return
        }

        commentTypeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'CommentType.label', default: 'CommentType'), commentTypeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'commentType.label', default: 'CommentType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
