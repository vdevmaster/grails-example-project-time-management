package project.time.management

class TaskTime {

    Date startTime
    Date endTime

    Long time = 10

    Date dateCreated
    Date lastUpdated

    static belongsTo = [task: Task]

    static constraints = {
        task nullable: true
        startTime nullable: true
        endTime nullable: true
    }


    public Long timeToHours() {

        return time / 60

    }

    public Long timeToMinutes() {
        if (time < 60) {
            return time
        }
        return time % 60
    }


    public setTimeFromHoursAndMinutes(Long hours, Long minutes) {

        Long hoursToMinutes
        Long simpleMinutes

        if (hours == null || hours < 1) {
            hoursToMinutes = 0
        } else {
            hoursToMinutes = hours * 60
        }

        if (minutes == null || minutes < 0) {
            simpleMinutes = 0
        } else {
            simpleMinutes = minutes
        }

        time = hoursToMinutes +simpleMinutes
    }

    public static String displayTime(Long time) {
        if (time < 60) {
            return "$time м"
        }
        Long hours = time / 60
        Long minutes = time % 60
        return "$hours ч : $minutes м"
    }


}
