package project.time.management

class CommentType {

    String title
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }
}
