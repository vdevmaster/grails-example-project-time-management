package project.time.management

class Step {

    String title
    String body
    Integer orderNumber = 1
    Date startDate
    Date endDate
    Date dateCreated
    Date lastUpdated

    static belongsTo = [project: Project, status: Status]
    static hasMany = [comments: Comment, tasks: Task]

    static constraints = {
        comments nullable: true
        tasks nullable: true
        status nullable: true
    }


    String toString() {
        return title
    }

    public Long getTotalCompleteTasksTime() {
        Long totalTime = 0

        def t = Task.find("from Task t where t.step = ? and t.statusType.number = ?", this, TaskStatus.COMPLETE_NUMBER)

        if (t == null) {
            return totalTime
        }

        t.each {
            totalTime + it.getTotalTaskTime()
        }

        return totalTime
    }

    public Long getTotalTaskTime() {
        Long totalTime = 0
        if (tasks == null) {
            return totalTime
        }
        tasks.each {
            totalTime += it.getTotalTaskTime()
        }
        return totalTime
    }

    public Long getTotalTargetTime() {
        Long targetTime = 0
        if (tasks == null) {
            return targetTime
        }
        tasks.each {
            targetTime += it.targetTime
        }
        return targetTime
    }

}
