package project.time.management

class Task {

    String title
    String body
    Long targetTime = 10
    Date dateCreated
    Date lastUpdated

    static belongsTo = [project: Project, step: Step, priority: Priority, taskType: TaskType, taskStatus: TaskStatus]
    static hasMany = [comments: Comment, taskTimes: TaskTime]


    public Long targetTimeToHours() {
        return targetTime / 60
    }


    public Long targetTimeToMinutes() {
        if (targetTime < 60) {
            return targetTime
        }
        return targetTime % 60
    }

    public void setTargetTimeFromHoursAndMiutes(Long hours, Long minutes) {
        Long hoursToMinutes
        Long simpleMinutes

        if (hours == null || hours < 1) {
            hoursToMinutes = 0
        } else {
            hoursToMinutes = hours * 60
        }

        if (minutes == null || minutes < 0) {
            simpleMinutes = 0
        } else {
            simpleMinutes = minutes
        }

        targetTime = hoursToMinutes +simpleMinutes
    }

    public Long getTotalTaskTime() {
        Long totalTaskTime = 0

        if (taskTimes == null) {
            return totalTaskTime
        }

        taskTimes.each {
            totalTaskTime += it.time
        }

        return totalTaskTime
    }

    static constraints = {
        body nullable: true

        project nullable: true
        step nullable: true
        priority nullable: true
        taskType nullable: true
        taskStatus nullable: true

        comments nullable: true
        taskTimes nullable: true
    }

    static mapping = {
        autoTimestamp true
    }

}
