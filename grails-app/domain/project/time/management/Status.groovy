package project.time.management

import project.time.management.StatusType

class Status {
    Date dateCreated
    Date lastUpdated

    static belongsTo = [statusType: StatusType, project: Project, step: Step]

    static constraints = {
        project nullable: true
        step nullable: true
    }

    public static Status createSaveAndUpdateProjectStatus(Project project, String statusTitle) {
        def statusType = StatusType.findByTitle(statusTitle)
        def status = new Status()
        status.project = project
        status.statusType = statusType
        status.save()
        project.status = status
        project.save()
        return status
    }

    public static Status updateProjectStatus(Project project, String statusTypeTitle) {
        def statusType = StatusType.findByTitle(statusTypeTitle)
        def status = Status.get(project.status.id)
        status.statusType = statusType
        status.save()
        return status
    }


    public static Status createSaveAndUpdateStepStatus(Step step, String statusTitle) {
        def statusType = StatusType.findByTitle(statusTitle)
        def status = new Status()
        status.step = step
        status.statusType = statusType
        status.save()
        step.status = status
        step.save()
        return status
    }

    public static Status updateStepStatus(Step step, String statusTypeTitle) {
        def statusType = StatusType.findByTitle(statusTypeTitle)
        def status = Status.get(step.status.id)
        status.statusType = statusType
        status.save()
        return status
    }

}
