package project.time.management

class Priority {

    String title
    Integer number = 1
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    String toString(){
        return title
    }

    public static final String VERY_LOW_PRIORITY="Очень низкий"
    public static final String LOW_PRIORITY="Низкий"
    public static final String MEDIUM_PRIORITY="Обычный"
    public static final String HIGH_PRIORITY="Высокий"
    public static final String CRITICAL_PRIORITY="Критичный"

    public static final Long VERY_LOW_PRIORITY_NUMBER =1
    public static final Long LOW_PRIORITY_NUMBER=2
    public static final Long MEDIUM_PRIORITY_NUMBER=3
    public static final Long HIGH_PRIORITY_NUMBER=4
    public static final Long CRITICAL_PRIORITY_NUMBER=5

}
