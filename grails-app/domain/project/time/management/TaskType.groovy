package project.time.management

class TaskType {

    String title
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    String toString(){
        return title
    }
    public static final String PLANNED = "Запланированная"
    public static final String EXTRA_ASKED = "Дополнительная"
    public static final String EXTRA_NOT_ASKED = "Непредвиденная"
    public static final String BUG_FIX = "Исправление"
}
