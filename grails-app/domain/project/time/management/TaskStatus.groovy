package project.time.management

class TaskStatus {

    String title
    String body
    Long number
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }
    String toString(){
        return title
    }

    public static final String NEED_WORK_ON_IT ="Требует выполнение"
    public static final String IN_WORK ="В обработке"
    public static final String COMPLETE ="Завершенна"
    public static final String IN_WORK_FROZEN ="В обработке - Приостановлена"
    public static final String IN_WORK_DROPPED ="В обработке - Брошенна"
    public static final String FEATURE_NOT_WORK_ON_IT ="Возможная задача "
    public static final String NOT_NEED_ANYMORE ="Не актуальна"

    public static final Long NEED_WORK_ON_IT_NUMBER =0
    public static final Long IN_WORK_NUMBER =1
    public static final Long COMPLETE_NUMBER =2
    public static final Long IN_WORK_FROZEN_NUMBER =3
    public static final Long IN_WORK_DROPPED_NUMBER =4
    public static final Long FEATURE_NOT_WORK_ON_IT_NUMBER =5
    public static final Long NOT_NEED_ANYMORE_NUMBER =6

}
