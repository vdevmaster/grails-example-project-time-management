package project.time.management

class StatusType {

    String title
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    public static final String ACTIVE="Активный"
    public static final String FROZEN="Остановлен"
    public static final String COMPLETE="Завершён"

    String  toString(){
    return title
    }
}
