package project.time.management

class Project {

String title
String body
String target

Date startDate
Date endDate

    Date dateCreated
    Date lastUpdated

static hasMany = [comments:Comment,steps:Step,task:Task]

static belongsTo = [user:User,status:Status]


    static constraints = {
    
    comments nullable: true
    status nullable: true
    user nullable:true
    
    }

    String toString(){
        return title
    }

    public Long getTotalTaskTime() {

        Long totalTime = 0

        if (steps == null) {
        return totalTime
        }

        steps.each {
            totalTime+= it.getTotalTaskTime()
        }

        return totalTime
    }

    public Long getTotalTargetTime() {

        Long targetTime = 0

        if (steps == null) {
            return targetTime
        }

        steps.each {
            targetTime+= it.getTotalTargetTime()
        }

        return targetTime
    }

}
