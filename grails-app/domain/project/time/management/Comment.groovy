package project.time.management

class Comment {

    String body
        
    Long targetId

    Date dateCreated
    Date lastUpdated

    static belongsTo = [commentType:CommentType,user:User]

    static constraints = {
    }
}
