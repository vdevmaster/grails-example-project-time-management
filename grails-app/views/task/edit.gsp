<%@ page import="project.time.management.Task" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'task.label', default: 'Task')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#edit-task" class="skip" tabindex="-1">
		<g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
				<li><g:link class="list" action="index" params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id]">Список задач для этапа</g:link></li>
                				<li>
                				<g:link class="create" action="create" params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id]">Добавить задачу
                				</g:link></li></ul>
		</div>
		<div id="edit-task" class="content scaffold-edit" role="main">
				<h1>Редактирование задачи для проекта
            	<g:link action="show" controller="project"  params="[projectId:taskInstance.project.id]">${taskInstance.project.title}</g:link>
            	</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${taskInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${taskInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:taskInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${taskInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="Обновить" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
