<%@ page import="project.time.management.Task" %>
<%@ page import="project.time.management.TaskTime" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'task.label', default: 'Task')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-task" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:render template="/base/home_link"/></li>
        <li><g:link class="list" action="index"
                    params="[projectId: taskInstance.project.id, stepId: taskInstance.step.id]">Список задач для этапа</g:link></li>
        <li>
            <g:link class="create" action="create"
                    params="[projectId: taskInstance.project.id, stepId: taskInstance.step.id]">Добавить задачу
            </g:link></li>
    </ul>
</div>

<div id="show-task" class="content scaffold-show" role="main">
    <h1>Задача для проекта
    <g:link controller="project" action="show"
            params="[projectId: taskInstance?.project?.id]">${taskInstance?.project?.title.encodeAsHTML()}
    </g:link>
    и этапа
    <g:link controller="step" action="show"
            params="[projectId: taskInstance?.project?.id, stepId: taskInstance?.step?.id]">
        ${taskInstance?.step?.title.encodeAsHTML()}
    </g:link>
    </h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list task">

        <g:if test="${taskInstance?.step}">
            <li class="fieldcontain">
                <span id="step-label" class="property-label">Этап</span>
                <span class="property-value" aria-labelledby="step-label">
                    <g:link controller="step" action="show"
                            params="[projectId: taskInstance.project.id, stepId: taskInstance.step.id]">${taskInstance?.step?.title.encodeAsHTML()}</g:link>
                </span>
            </li>
        </g:if>

        <g:if test="${taskInstance?.title}">
            <li class="fieldcontain">
                <span id="title-label" class="property-label">Название</span>
                <span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${taskInstance}"
            </li>
        </g:if>

        <g:if test="${taskInstance?.body}">
            <li class="fieldcontain">
                <span id="body-label" class="property-label">Описание</span>
                <span class="property-value" aria-labelledby="body-label"><g:fieldValue bean="${taskInstance}"
            </li>
        </g:if>

        <g:if test="${taskInstance?.taskStatus}">
            <li class="fieldcontain">
                <span id="status-label" class="property-label">Статус</span>
                <span class="property-value" aria-labelledby="status-label">
                    ${taskInstance?.taskStatus?.title}
                </span>
            </li>
        </g:if>

        <g:if test="${taskInstance?.priority}">
            <li class="fieldcontain">
                <span id="priority-label" class="property-label">Приоритет</span>
                <span class="property-value" aria-labelledby="priority-label">
                    ${taskInstance?.priority?.title.encodeAsHTML()}
                </span>

            </li>
        </g:if>

        <g:if test="${taskInstance?.taskType}">
            <li class="fieldcontain">
                <span id="taskType-label" class="property-label">Тип задачи</span>

                <span class="property-value" aria-labelledby="taskType-label">

                    ${taskInstance?.taskType?.title.encodeAsHTML()}
                </span>
            </li>
        </g:if>

        <g:if test="${taskInstance?.comments}">
            <li class="fieldcontain">
                <span id="comments-label" class="property-label"><g:message code="task.comments.label"
                                                                            default="Comments"/></span>

                <g:each in="${taskInstance.comments}" var="c">
                    <span class="property-value" aria-labelledby="comments-label"><g:link controller="comment"
                                                                                          action="show"
                                                                                          id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
                </g:each>

            </li>
        </g:if>


        <li class="fieldcontain">
            <span id="targetTime-label" class="property-label">Время на выполнение</span>

            <span class="property-value" aria-labelledby="targetTime-label">
                ${TaskTime.displayTime(taskInstance.targetTime)}
            </span>

        </li>

        <li class="fieldcontain">
            <span id="totalTaskTime-label" class="property-label">Затрачено времени:</span>
            <span class="property-value" aria-labelledby="targetTime-label">
                ${TaskTime.displayTime(taskInstance.getTotalTaskTime())}
            </span>

        </li>

        <g:if test="${taskInstance?.taskTimes}">

            <li class="fieldcontain">
                <span class="property-label">Затраты времени</span>
                <span class="property-value">
                    <ol>
                        <g:each var="taskTime" in="${taskInstance.taskTimes}">
                            <li><g:link action="show" controller="taskTime"
                                        params="[projectId: taskInstance.project.id, stepId: taskInstance.step.id, taskId: taskInstance.id, taskTimeId: taskTime.id]">
                                ${TaskTime.displayTime(taskTime.time)} -  ${taskTime.lastUpdated}</g:link>
                            </li>
                        </g:each>
                    </ol>
                </span>
            </li>
        </g:if>

    </ol>
    <g:form url="[resource: taskInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit"
                    params="[projectId: taskInstance.project.id, stepId: taskInstance.step.id, taskId: taskInstance.id]"><g:message
                    code="default.button.edit.label" default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
            <g:link class="add" action="create" controller="taskTime"
                    params="[projectId: taskInstance.project.id, stepId: taskInstance.step.id, taskId: taskInstance.id]">Добавить время</g:link>
        </fieldset>
    </g:form>
</div>
</body>
</html>
