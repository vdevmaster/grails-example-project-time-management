
<%@ page import="project.time.management.Task" %>
<%@ page import="project.time.management.TaskTime" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'task.label', default: 'Task')}" />
		<title>Список задач для проекта ${}</title>
	</head>
	<body>
		<a href="#list-task" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
				<li><g:link class="list" action="index" params="[projectId:projectInstance.id,stepId:stepInstance.id]">Список задач для этапа</g:link></li>
				<li>
				<g:link class="create" params="[projectId:projectInstance.id,stepId:stepInstance.id]" action="create">Добавить задачу
				</g:link>
				</li>
			</ul>
		</div>
		<div id="list-task" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					<th><g:sortableColumn params="[projectId:projectInstance.id,stepId:stepInstance.id]" property="priority" title="Приоритет" /></th>
					<th><g:sortableColumn params="[projectId:projectInstance.id,stepId:stepInstance.id]" property="title" title="Название" /></th>
					<th><g:sortableColumn params="[projectId:projectInstance.id,stepId:stepInstance.id]" property="targetTime" title="Целевое время" /></th>
					<th><g:sortableColumn params="[projectId:projectInstance.id,stepId:stepInstance.id]" property="totalTaskTime" title="Затраченное" /></th>
					<th><g:sortableColumn params="[projectId:projectInstance.id,stepId:stepInstance.id]" property="status" title="Статус" /></th>


							%{--
					<th><g:sortableColumn property="type" title="Тип" /></th>
					<th><g:sortableColumn property="body" title="Описание" /></th>
					<th><g:sortableColumn property="lastUpdated" title="Последние изменение" /></th>
					--}%
					</tr>
				</thead>
				<tbody>
				<g:each in="${taskInstanceList}" status="i" var="taskInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td>${taskInstance.priority.title}</td>
<td><g:link action="show" params="[projectId:projectInstance.id,stepId:stepInstance.id,taskId:taskInstance.id]">${taskInstance.title}</g:link></td>

						<td>${TaskTime.displayTime(taskInstance.targetTime)}</td>
						<td>${TaskTime.displayTime(taskInstance.getTotalTaskTime())}</td>
					    <td>${taskInstance.taskStatus.title}</td>
					%{--
						<td>${taskInstance.taskType.title}</td>
						<td>${fieldValue(bean: taskInstance, field: "body")}</td>
                        <td>${taskInstance.lastUpdated}</td>
					--}%
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate params="[projectId:projectInstance.id,stepId:stepInstance.id]" total="${taskInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
