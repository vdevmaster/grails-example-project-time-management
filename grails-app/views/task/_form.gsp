<%@ page import="project.time.management.Task" %>
<%@ page import="project.time.management.Step" %>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'step', 'error')} ">
	<label for="step">
	Этап
	</label>
	<g:select id="step" name="step.id" from="${project.time.management.Step.list()}" optionKey="id" value="${taskInstance?.step?.id}" class="many-to-one" />

</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="task.title.label" default="Название задачи" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${taskInstance?.title}"/>


<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'body', 'error')} required">
	<label for="body">
		<g:message code="task.body.label" default="Описание задачи" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="body" required="" value="${taskInstance?.body}"/>

</div>

<div class="fieldcontain">
	<label for="statusType">
		<g:message code="project.status.label" default="Статус задачи" />
		<span class="required-indicator">*</span>
	</label>
    <g:select id="taskStatusNumber" name="taskStatusNumber" from="${project.time.management.TaskStatus.list()}" optionKey="number" required="" value="${taskInstance?.taskStatus?.number}" class=""/>

</div>


<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'taskType', 'error')} ">
	<label for="taskType">
		<g:message code="task.taskType.label" default="Тип задачи" />

	</label>
	<g:select id="taskTypeTitle" name="taskTypeTitle" from="${project.time.management.TaskType.list()}" optionKey="title" value="${taskInstance?.taskType?.title}" class="many-to-one" />

</div>


<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'priority', 'error')} ">
	<label for="priority">
		<g:message code="task.priority.label" default="Приоритет задачи" />
		
	</label>
	<g:select id="priorityNumber" name="priorityNumber" from="${project.time.management.Priority.list()}" optionKey="number" value="${taskInstance?.priority?.number}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'targetTime', 'error')} required">
	<label for="targetTime">
		<g:message code="task.targetTime.label" default="Время на выполнение задачи" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number"  name="targetTimeHours"  min="0" max="24" value="${taskInstance?.targetTimeToHours()}"  /> ч :
    <g:field type="number"  min="0" max="60" name="targetTimeMinutes" value="${taskInstance?.targetTimeToMinutes()}"  /> мм

</div>

<g:hiddenField  name="projectId" value="${taskInstance.project.id}"/>
<g:hiddenField  name="stepId" value="${taskInstance.step.id}"/>

<g:if test="${taskInstance?.comments != null}"
<div class="fieldcontain ${hasErrors(bean: taskInstance, field: 'comments', 'error')} ">
	<label for="comments">
		<g:message code="task.comments.label" default="Комментарии" />

	</label>

</div>
</g:if>