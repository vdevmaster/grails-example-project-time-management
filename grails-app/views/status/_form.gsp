<%@ page import="project.time.management.Status" %>



<div class="fieldcontain ${hasErrors(bean: statusInstance, field: 'project', 'error')} ">
	<label for="project">
		<g:message code="status.project.label" default="Project" />
		
	</label>
	<g:select id="project" name="project.id" from="${project.time.management.Project.list()}" optionKey="id" value="${statusInstance?.project?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: statusInstance, field: 'step', 'error')} ">
	<label for="step">
		<g:message code="status.step.label" default="Step" />
		
	</label>
	<g:select id="step" name="step.id" from="${project.time.management.Step.list()}" optionKey="id" value="${statusInstance?.step?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: statusInstance, field: 'statusType', 'error')} required">
	<label for="statusType">
		<g:message code="status.statusType.label" default="Status Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="statusType" name="statusType.id" from="${project.time.management.StatusType.list()}" optionKey="id" required="" value="${statusInstance?.statusType?.id}" class="many-to-one"/>

</div>

