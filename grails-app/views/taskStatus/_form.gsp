<%@ page import="project.time.management.TaskStatus" %>



<div class="fieldcontain ${hasErrors(bean: taskStatusInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="taskStatus.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${taskStatusInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: taskStatusInstance, field: 'number', 'error')} required">
	<label for="number">
		<g:message code="taskStatus.number.label" default="Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="number" type="number" value="${taskStatusInstance.number}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: taskStatusInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="taskStatus.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${taskStatusInstance?.title}"/>

</div>

