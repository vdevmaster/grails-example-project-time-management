<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'taskTime.label', default: 'TaskTime')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-taskTime" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
			<li><g:link class="list" params="[projectId:taskTimeInstance.task.step.project.id,stepId:taskTimeInstance.task.step.id,taskId:taskTimeInstance.task.id]" action="index">Список всех затрат времени для задачи</g:link></li>
            					</ul>
		</div>
		<div id="create-taskTime" class="content scaffold-create" role="main">
			<h1>
			Добавить время для задачи <g:link controller="task" action="show" params="[projectId:taskTimeInstance.task.project.id,stepId:taskTimeInstance.task.step.id,taskId:taskTimeInstance.task.id]">
			${taskTimeInstance.task?.title}
			</g:link>
			</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${taskTimeInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${taskTimeInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:taskTimeInstance, action:'save']" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
