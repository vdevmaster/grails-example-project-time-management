<%@ page import="project.time.management.TaskTime" %>

<div class="fieldcontain ${hasErrors(bean: taskTimeInstance, field: 'userTime', 'error')} ">
	<label for="userTime">
		Итоговое время
	</label>
	<g:field type="number" min="0" max="120"  name="timeHours"    value="${taskTimeInstance?.timeToHours()}"  /> ч :
	<g:field type="number" min="0" max="60" name="timeMinutes"   value="${taskTimeInstance?.timeToMinutes()}" /> м
</div>

<g:hiddenField name="projectId" value="${taskTimeInstance.task.project.id}" />
<g:hiddenField name="stepId" value="${taskTimeInstance.task.step.id}" />
<g:hiddenField name="taskId" value="${taskTimeInstance.task.id}" />
