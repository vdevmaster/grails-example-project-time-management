
<%@ page import="project.time.management.TaskTime" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'taskTime.label', default: 'TaskTime')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-taskTime" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
				<li><g:link class="list" params="[projectId:taskTimeInstance.task.step.project.id,stepId:taskTimeInstance.task.step.id,taskId:taskTimeInstance.task.id]" action="index">Список всех затрат времени для задачи</g:link></li>
				<li><g:link class="create" params="[projectId:taskTimeInstance.task.step.project.id,stepId:taskTimeInstance.task.step.id,taskId:taskTimeInstance.task.id]" action="create">Добавить время для задачи</g:link></li>
			</ul>
		</div>
		<div id="show-taskTime" class="content scaffold-show" role="main">
			<h1>Время для задачи <g:link controller="task" action="show" params="[projectId:taskTimeInstance.task.step.project.id,stepId:taskTimeInstance.task.step.id,taskId:taskTimeInstance.task.id]">${taskTimeInstance.task.title}</g:link></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list taskTime">
				<g:if test="${taskTimeInstance?.time}">
				<li class="fieldcontain">
					<span id="time-label" class="property-label">Затраченое время</span>

						<span class="property-value" aria-labelledby="time-label">
						${TaskTime.displayTime(taskTimeInstance?.time)}
						</span>
					
				</li>
				</g:if>
			</ol>
			<g:form url="[resource:taskTimeInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" params="[projectId:taskTimeInstance.task.project.id,stepId:taskTimeInstance.task.step.id,taskId:taskTimeInstance.task.id,taskTimeId:taskTimeInstance.id]">
					<g:message code="default.button.edit.label" default="Edit" />
					</g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
