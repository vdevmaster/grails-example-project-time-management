
<%@ page import="project.time.management.TaskTime" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'taskTime.label', default: 'TaskTime')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-taskTime" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
				<li><g:link class="list" params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id,taskId:taskInstance.id]" action="index">Список всех затрат времени для задачи</g:link></li>
				<li><g:link class="create" params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id,taskId:taskInstance.id]" action="create">Добавить время для задачи</g:link></li>

			</ul>
		</div>
		<div id="list-taskTime" class="content scaffold-list" role="main">
			<h1>Список всех затрат времени для <g:link action="show" controller="task" params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id,taskId:taskInstance.id]">${taskInstance.title}</g:link></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
						<g:sortableColumn params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id,taskId:taskInstance.id]" property="id" title="Id" />
						<g:sortableColumn params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id,taskId:taskInstance.id]" property="time" title="Время" />
						<g:sortableColumn params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id,taskId:taskInstance.id]" property="lastUpdated" title="Последние изменение" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${taskTimeInstanceList}" status="i" var="taskTimeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td><g:link action="show" params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id,taskId:taskInstance.id,taskTimeId:taskTimeInstance.id]">
						${fieldValue(bean: taskTimeInstance, field: "id")}</g:link>
						</td>

						<td>${TaskTime.displayTime(taskTimeInstance.time)}</td>
						<td>${taskTimeInstance.lastUpdated}</td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate params="[projectId:taskInstance.project.id,stepId:taskInstance.step.id,taskId:taskInstance.id]" total="${taskTimeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
