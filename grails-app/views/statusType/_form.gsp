<%@ page import="project.time.management.StatusType" %>



<div class="fieldcontain ${hasErrors(bean: statusTypeInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="statusType.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${statusTypeInstance?.title}"/>

</div>

