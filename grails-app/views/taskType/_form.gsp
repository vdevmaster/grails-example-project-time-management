<%@ page import="project.time.management.TaskType" %>



<div class="fieldcontain ${hasErrors(bean: taskTypeInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="taskType.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${taskTypeInstance?.title}"/>

</div>

