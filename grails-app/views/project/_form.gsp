<%@ page import="project.time.management.Project" %>


<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="project.title.label" default="Название проекта" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${projectInstance?.title}"/>

</div>


<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'target', 'error')} required">
	<label for="target">
		<g:message code="project.target.label" default="Цель проекта" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="target" required="" value="${projectInstance?.target}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'body', 'error')} required">
	<label for="body">
		<g:message code="project.body.label" default="Описание проекта" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="body" required="" value="${projectInstance?.body}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'startDate', 'error')} required">
	<label for="startDate">
		<g:message code="project.startDate.label" default="Начало проекта" />
		<span class="required-indicator">*</span>
	</label>
	
	<g:datePicker name="startDate" value="${projectInstance?.startDate}"
              default="${new Date()}"/>
              
</div>



<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'endDate', 'error')} required">
	<label for="endDate">
		<g:message code="project.endDate.label" default="Окончание проекта" />
		<span class="required-indicator">*</span>
	</label>
	
	<g:datePicker name="endDate" value="${projectInstance?.endDate}"
              default="${new Date().plus(14)}"/>
              
</div>



<div class="fieldcontain ">
	<label for="statusTypeTitle">
		Статус проекта
		<span class="required-indicator">*</span>
	</label>

	<g:select id="statusTypeTitle" name="statusTypeTitle" from="${project.time.management.StatusType.list()}" optionKey="title" required="" value="${projectInstance?.status?.statusType?.title}" class=""/>

</div>

