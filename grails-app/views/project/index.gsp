
<%@ page import="project.time.management.Project" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'project.label', default: 'Project')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-project" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<g:render template="projects_links"/>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-project" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>


			<thead>
					<tr>


					<g:if test="${ typeProject == "onlyActive"}">
<g:sortableColumn params="[onlyActive:true]" property="title" title="${message(code: 'project.title.label', default: 'Название')}" />
<g:sortableColumn params="[onlyActive:true]" property="target" title="${message(code: 'project.target.label', default: 'Цель')}" />
<g:sortableColumn params="[onlyActive:true]" property="body" title="${message(code: 'project.body.label', default: 'Описание')}" />
<g:sortableColumn params="[onlyActive:true]" property="startDate" title="${message(code: 'project.body.label', default: 'Начало')}" />
<g:sortableColumn params="[onlyActive:true]" property="endDate" title="${message(code: 'project.body.label', default: 'Конец')}" />
		<g:sortableColumn params="[onlyActive:true]" property="lastUpdated" title="Изменён" />

								</g:if>
					<g:if test="${ typeProject == "onlyFrozen"}">
<g:sortableColumn params="[onlyFrozen:true]" property="title" title="${message(code: 'project.title.label', default: 'Название')}" />
<g:sortableColumn params="[onlyFrozen:true]" property="target" title="${message(code: 'project.target.label', default: 'Цель')}" />
                 <g:sortableColumn params="[onlyFrozen:true]" property="body" title="${message(code: 'project.body.label', default: 'Описание')}" />
                 <g:sortableColumn params="[onlyFrozen:true]" property="startDate" title="${message(code: 'project.body.label', default: 'Начало')}" />
                 <g:sortableColumn params="[onlyFrozen:true]" property="endDate" title="${message(code: 'project.body.label', default: 'Конец')}" />
                 <g:sortableColumn params="[onlyFrozen:true]" property="lastUpdated" title="Изменён" />
                    </g:if>
                    <g:if test="${ typeProject == "onlyCompleted"}">
<g:sortableColumn params="[onlyCompleted:true]" property="title" title="${message(code: 'project.title.label', default: 'Название')}" />
<g:sortableColumn params="[onlyCompleted:true]" property="target" title="${message(code: 'project.target.label', default: 'Цель')}" />
<g:sortableColumn params="[onlyCompleted:true]" property="body" title="${message(code: 'project.body.label', default: 'Описание')}" />
<g:sortableColumn params="[onlyCompleted:true]" property="startDate" title="${message(code: 'project.body.label', default: 'Начало')}" />
<g:sortableColumn params="[onlyCompleted:true]" property="endDate" title="${message(code: 'project.body.label', default: 'Конец')}" />
<g:sortableColumn params="[onlyCompleted:true]" property="lastUpdated" title="Изменён" />
                    </g:if>

					</tr>
				</thead>
				<tbody>
				<g:each in="${projectInstanceList}" status="i" var="projectInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="show" params="[projectId:projectInstance.id]">${fieldValue(bean: projectInstance, field: "title")}</g:link></td>
				        <td>${fieldValue(bean: projectInstance, field: "target")}</td>
						<td>${fieldValue(bean: projectInstance, field: "body")}</td>
						<td>${fieldValue(bean: projectInstance, field: "startDate")}</td>
						<td>${fieldValue(bean: projectInstance, field: "endDate")}</td>
						<td>${fieldValue(bean: projectInstance, field: "lastUpdated")}</td>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">

	<g:if test="${ typeProject == "onlyActive"}">
	<g:paginate params="[onlyActive:true]" total="${projectInstanceCount ?: 0}" />
</g:if>

			<g:if test="${ typeProject == "onlyFrozen"}">
				<g:paginate params="[onlyFrozen:true]" total="${projectInstanceCount ?: 0}" />
			</g:if>

			<g:if test="${ typeProject == "onlyCompleted"}">
			<g:paginate params="[onlyCompleted:true]" total="${projectInstanceCount ?: 0}" />
			 </g:if>

			</div>
		</div>
	</body>
</html>
