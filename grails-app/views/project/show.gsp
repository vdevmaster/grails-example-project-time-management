
<%@ page import="project.time.management.Project" %>
<%@ page import="project.time.management.TaskTime" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'project.label', default: 'Проект')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-project" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
				<g:render template="projects_links"/>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-project" class="content scaffold-show" role="main">
			<h1>Проект ${projectInstance.title}</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list project">
			
				<g:if test="${projectInstance?.comments}">
				<li class="fieldcontain">
					<span id="comments-label" class="property-label"><g:message code="project.comments.label" default="Comments" /></span>
					
						<g:each in="${projectInstance.comments}" var="c">
						<span class="property-value" aria-labelledby="comments-label"><g:link controller="comment" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			
			
			       <g:if test="${projectInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="project.title.label" default="Название проекта" /></span>
					
						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${projectInstance}" field="title"/></span>
					
				</li>
				</g:if>
			
			
			       <g:if test="${projectInstance?.target}">
				<li class="fieldcontain">
					<span id="target-label" class="property-label"><g:message code="project.target.label" default="Цель проекта" /></span>
					
						<span class="property-value" aria-labelledby="target-label"><g:fieldValue bean="${projectInstance}" field="target"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${projectInstance?.body}">
				<li class="fieldcontain">
					<span id="body-label" class="property-label"><g:message code="project.body.label" default="Описание проекта" /></span>
					
						<span class="property-value" aria-labelledby="body-label"><g:fieldValue bean="${projectInstance}" field="body"/></span>
					
				</li>
				</g:if>
				
				
				<g:if test="${projectInstance?.startDate}">
				<li class="fieldcontain">
					<span id="startDate-label" class="property-label"><g:message code="project.body.label" default="Начало проекта" /></span>
					
						<span class="property-value" aria-labelledby="startDate-label"><g:fieldValue bean="${projectInstance}" field="startDate"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${projectInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="project.body.label" default="Окончание проекта" /></span>
					
						<span class="property-value" aria-labelledby="endDate-label"><g:fieldValue bean="${projectInstance}" field="endDate"/></span>
					
				</li>
				</g:if>
				

				
			
			
				<g:if test="${projectInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label">Статус</span>
					
						<span class="property-value" aria-labelledby="status-label">
					${projectInstance?.status?.statusType?.title.encodeAsHTML()}</span>
					
				</li>
				</g:if>


<g:if test="${projectInstance?.steps}">
	<li class="fieldcontain">
<span class="property-label">Целевое время всех задач:</span>

<span class="property-value" >
${TaskTime.displayTime(projectInstance?.getTotalTargetTime())}
	</span>
		</li>
</g:if>

<g:if test="${projectInstance?.steps}">
<li class="fieldcontain">
<span class="property-label">Фактическое время всех задач:</span>

<span class="property-value" >
${TaskTime.displayTime(projectInstance?.getTotalTaskTime())}
	</span>
	</li>
</g:if>

				<g:if test="${projectInstance?.steps}">
            				<li class="fieldcontain">

            		<span class="property-label">Этапы</span>


            		<span class="property-value" >
            				<ol >
            				    <g:each var="step" in="${projectInstance.steps}" >
            					<li >
            					<g:link action="show" controller="step" params="[projectId:projectInstance.id,stepId:step.id]">
            					${step.title} - Статус: ${step.status.statusType.title} - Начало: ${step.startDate} - Конец: ${step.endDate}
            					</g:link>
            					</li>
            					</g:each>
            				</ol>
            				</li>
					</span>
            				</g:if>

			
			</ol>
			<g:form url="[resource:projectInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${projectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />

					<g:if test="${displayCreateButton}">
					<g:link class="create" action="create" controller="step" params="[projectId:projectInstance.id]">Добавить этап</g:link>
					</g:if>
				</fieldset>
			</g:form>

		</div>
	</body>
</html>
