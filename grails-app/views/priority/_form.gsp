<%@ page import="project.time.management.Priority" %>



<div class="fieldcontain ${hasErrors(bean: priorityInstance, field: 'number', 'error')} required">
	<label for="number">
		<g:message code="priority.number.label" default="Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="number" type="number" value="${priorityInstance.number}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: priorityInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="priority.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${priorityInstance?.title}"/>

</div>

