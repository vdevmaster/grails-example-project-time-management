<%@ page import="project.time.management.Step" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'step.label', default: 'Step')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#edit-step" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
				<li><g:link class="list" action="index" params="[projectId:stepInstance.project.id]" >Список этапов</g:link></li>
			<li><g:link class="create" action="create" params="[projectId:stepInstance.project.id]">Добавить этап</g:link></li>
			</ul>
		</div>
		<div id="edit-step" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${stepInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${stepInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:stepInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${stepInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="Изменить" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
