<%@ page import="project.time.management.Step" %>

<div class="fieldcontain ${hasErrors(bean: stepInstance, field: 'orderNumber', 'error')} required">
	<label for="orderNumber">
		<g:message code="step.orderNumber.label" default="Номер этапа" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="orderNumber" type="number" value="${stepInstance?.orderNumber}" required=""/>
</div>


<div class="fieldcontain ${hasErrors(bean: stepInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="step.title.label" default="Название этапа" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${stepInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stepInstance, field: 'body', 'error')} required">
	<label for="body">
		<g:message code="step.body.label" default="Описание этапа" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="body" required="" value="${stepInstance?.body}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: stepInstance, field: 'startDate', 'error')} required">
	<label for="startDate">
		<g:message code="project.startDate.label" default="Начало этапа" />
		<span class="required-indicator">*</span>
	</label>
	
	<g:datePicker name="startDate" value="${stepInstance?.startDate}"
              default="${stepInstance?.project?.startDate}"/>
              
</div>

<div class="fieldcontain ${hasErrors(bean: stepInstance, field: 'endDate', 'error')} required">
	<label for="endDate">
		<g:message code="project.endDate.label" default="Окончание этапа" />
		<span class="required-indicator">*</span>
	</label>
	
	<g:datePicker name="endDate" value="${stepInstance?.endDate}"
              default="${stepInstance?.project?.endDate}"/>
              
</div>

<g:if test="${stepInstance!=null}">
<g:hiddenField name="projectId" value="${stepInstance.project.id}"/>
</g:if>
<g:else>
<g:hiddenField name="projectId" value="${projectId}"/>
</g:else>

<div class="fieldcontain">
	<label for="statusType">
		<g:message code="project.status.label" default="Статус этапа" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="statusTypeTitle" name="statusTypeTitle" from="${project.time.management.StatusType.list()}" optionKey="title" required="" value="${stepInstance?.status?.statusType?.title}" class=""/>

</div>
