
<%@ page import="project.time.management.Step" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'step.label', default: 'Step')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-step" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
				<li><g:link class="create" action="create" params="[projectId:projectInstance.id]">Добавить этап</g:link></li>
			</ul>
		</div>
		<div id="list-step" class="content scaffold-list" role="main">
			<h1>Список этапов для проекта
			<g:link action="show" controller="project" params="[projectId:projectInstance.id]">
			${projectInstance.title}
			</g:link>
			</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

					    <g:sortableColumn params="[projectId:projectInstance.id]" property="orderNumber" title="Номер" />

				    	<g:sortableColumn params="[projectId:projectInstance.id]" property="title" title="Название" />

						<g:sortableColumn params="[projectId:projectInstance.id]" property="body" title="Описание" />

						<th>Статус</th>

                        <g:sortableColumn params="[projectId:projectInstance.id]" property="startDate" title="Начало" />

						<g:sortableColumn params="[projectId:projectInstance.id]" property="endDate" title="Конец" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${projectInstance.steps}" status="i" var="stepInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td>${fieldValue(bean: stepInstance, field: "orderNumber")}</td>
						<td><g:link action="show" params="[projectId:projectInstance.id,stepId:stepInstance.id]">${fieldValue(bean: stepInstance, field: "title")}</g:link></td>
						<td>${fieldValue(bean: stepInstance, field: "body")}</td>
					    <td>${stepInstance?.status?.statusType?.title}</td>
						<td><g:formatDate date="${stepInstance.startDate}" /></td>
						<td><g:formatDate date="${stepInstance.endDate}" /></td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate params="[projectId:projectInstance.id]" total="${stepInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
