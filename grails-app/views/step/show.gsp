
<%@ page import="project.time.management.Step" %>
<%@ page import="project.time.management.TaskTime" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'step.label', default: 'Step')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-step" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:render template="/base/home_link"/></li>
				<li><g:link class="list" action="index" params="[projectId:stepInstance.project.id]" >Список этапов</g:link></li>
				<li><g:link class="create" action="create" params="[projectId:stepInstance.project.id]">Добавить этап</g:link></li>
			</ul>
		</div>
		<div id="show-step" class="content scaffold-show" role="main">
			<h1>Этап - ${stepInstance.title} для проекта
			<g:link action="show" controller="project" params="[projectId:stepInstance.project.id]">
			${stepInstance.project.title}
			</g:link>
			</h1>

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list step">

			<g:if test="${stepInstance?.project}">
            				<li class="fieldcontain">
            					<span id="project-label" class="property-label"><g:message code="step.project.label" default="Проект" /></span>

            						<span class="property-value" aria-labelledby="project-label">
            						<g:link controller="project" action="show" params="[projectId:stepInstance.project.id]">${stepInstance?.project?.encodeAsHTML()}</g:link>
            						</span>

            				</li>
            				</g:if>


			<g:if test="${stepInstance?.orderNumber}">
            				<li class="fieldcontain">
            					<span id="orderNumber-label" class="property-label"><g:message code="step.orderNumber.label" default="Номер этапа" /></span>

            						<span class="property-value" aria-labelledby="orderNumber-label"><g:fieldValue bean="${stepInstance}" field="orderNumber"/></span>

            				</li>
            				</g:if>



				<g:if test="${stepInstance?.title}">
            				<li class="fieldcontain">
            					<span id="title-label" class="property-label"><g:message code="step.title.label" default="Название этапа" /></span>

            						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${stepInstance}" field="title"/></span>

            				</li>
            				</g:if>

			
				<g:if test="${stepInstance?.body}">
				<li class="fieldcontain">
					<span id="body-label" class="property-label"><g:message code="step.body.label" default="Описание этапа" /></span>
					
						<span class="property-value" aria-labelledby="body-label"><g:fieldValue bean="${stepInstance}" field="body"/></span>
					
				</li>
				</g:if>

				            <g:if test="${stepInstance?.status}">
            				<li class="fieldcontain">
            					<span id="status-label" class="property-label"><g:message code="step.status.label" default="Состояние этапа" /></span>

            						<span class="property-value" aria-labelledby="status-label">${stepInstance?.status?.statusType?.title}</span>

            				</li>
            				</g:if>


                            				<li class="fieldcontain">
                            					<span id="status-active-label" class="property-label">
                            					<g:message code="step.status.label" default="Дата изменения статуса" /></span>

                            				    <span class="property-value" aria-labelledby="status-label">
                            				    ${stepInstance?.status?.lastUpdated}
                            				    </span>

                            				</li>

			<g:if test="${stepInstance?.startDate}">
            				<li class="fieldcontain">
            					<span id="startDate-label" class="property-label"><g:message code="step.startDate.label" default="Время начала этапа" /></span>

            						<span class="property-value" aria-labelledby="startDate-label"><g:formatDate date="${stepInstance?.startDate}" /></span>

            				</li>
            				</g:if>

				<g:if test="${stepInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="step.endDate.label" default="Время окончания этапа" /></span>

						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${stepInstance?.endDate}" /></span>
					
				</li>
				</g:if>
			
                          <li class="fieldcontain">
            					<span class="property-label">
            					Планируемое время на выполнение всех задач в этапе
            					</span>

                         <span class="property-value" >
                      ${TaskTime.displayTime(stepInstance.getTotalTargetTime())}
                       </span>
            				</li>


                          <li class="fieldcontain">
            					<span class="property-label">
            					Общее затраченое время на все этапы
            					</span>

                         <span class="property-value" >
                      ${TaskTime.displayTime(stepInstance.getTotalTaskTime())}
                       </span>
            				</li>

				<g:if test="${stepInstance?.tasks}">
            				<li class="fieldcontain">
            					<span id="tasks-label" class="property-label"><g:message code="step.tasks.label" default="Задачи" /></span>


									<span class="property-value" aria-labelledby="tasks-label">
            						<ol>
            						<g:each in="${stepInstance.tasks}" var="task">
            						<li>
            						<g:link controller="task" action="show" params="[projectId:stepInstance.project.id,stepId:stepInstance.id,taskId:task.id]">
            						  Название задачи: ${task.title} - Приоритет: ${task.priority.title} - Целевое время: ${TaskTime.displayTime(task.targetTime)} - Затраченое время: ${TaskTime.displayTime(task.getTotalTaskTime())} - Статус задачи: ${task.taskStatus.title}
            						</g:link>
            						</li>
            						</g:each>
                                     </ol>
                                     </span>
            				</li>
            				</g:if>


					<g:if test="${stepInstance?.comments}">
                				<li class="fieldcontain">
                					<span id="comments-label" class="property-label"><g:message code="step.comments.label" default="Comments" /></span>

                						<g:each in="${stepInstance.comments}" var="c">
                						<span class="property-value" aria-labelledby="comments-label">
                						<g:link controller="comment" action="show" id="${c.id}">
                						${c?.encodeAsHTML()}</g:link></span>
                						</g:each>

                				</li>
                				</g:if>

			</ol>
			<g:form url="[resource:stepInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${stepInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />

			        <g:if test="${displayCreateButton}">
			                <g:link class="add" action="create" controller="task" params="[projectId:stepInstance?.project?.id,stepId:stepInstance?.id]">Добавить задачу</g:link>
			        </g:if>

					</fieldset>
			</g:form>
		</div>
	</body>
</html>
