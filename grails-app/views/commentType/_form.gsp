<%@ page import="project.time.management.CommentType" %>



<div class="fieldcontain ${hasErrors(bean: commentTypeInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="commentType.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${commentTypeInstance?.title}"/>

</div>

