<%@ page import="project.time.management.Comment" %>



<div class="fieldcontain ${hasErrors(bean: commentInstance, field: 'body', 'error')} required">
	<label for="body">
		<g:message code="comment.body.label" default="Body" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="body" required="" value="${commentInstance?.body}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: commentInstance, field: 'commentType', 'error')} required">
	<label for="commentType">
		<g:message code="comment.commentType.label" default="Comment Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="commentType" name="commentType.id" from="${project.time.management.CommentType.list()}" optionKey="id" required="" value="${commentInstance?.commentType?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: commentInstance, field: 'targetId', 'error')} required">
	<label for="targetId">
		<g:message code="comment.targetId.label" default="Target Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="targetId" type="number" value="${commentInstance.targetId}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: commentInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="comment.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${project.time.management.User.list()}" optionKey="id" required="" value="${commentInstance?.user?.id}" class="many-to-one"/>

</div>

