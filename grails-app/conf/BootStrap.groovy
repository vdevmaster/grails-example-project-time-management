import project.time.management.TaskStatus
import project.time.management.User
import project.time.management.Role
import project.time.management.UserRole
import project.time.management.StatusType
import project.time.management.Priority
import project.time.management.TaskType

class BootStrap {

    def init = { servletContext ->


        def adminRole = Role.findByAuthority('ROLE_ADMIN')
        if (!adminRole) {
            adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        }

        def userRole = Role.findByAuthority('ROLE_USER')
        if (!userRole) {
            userRole = new Role(authority: 'ROLE_USER').save(flush: true)
        }

        def admin = User.findByUsername('admin')
        if (!admin) {
            admin = new User(username: 'admin', password: 'admin')
            admin.save(flush: true)
            UserRole.create admin, adminRole, true
        }

        def user = User.findByUsername('user')
        if (!user) {
            user = new User(username: 'user', password: 'pass')
            user.save(flush: true)
            UserRole.create user, userRole, true
        }


        println "Operation on StatusType..."

        def statusTypeActive = StatusType.findByTitle(StatusType.ACTIVE)
        if (!statusTypeActive) {
            statusTypeActive = new StatusType(title: StatusType.ACTIVE).save()
        }


        def statusTypeFrozen = StatusType.findByTitle(StatusType.FROZEN)
        if (!statusTypeFrozen) {
            statusTypeFrozen = new StatusType(title: StatusType.FROZEN).save()
        }


        def statusTypeComplete = StatusType.findByTitle(StatusType.COMPLETE)
        if (!statusTypeComplete) {
            statusTypeComplete = new StatusType(title: StatusType.COMPLETE).save()
        }


        println "Task Type"

        def taskTypePlanned= TaskType.findByTitle(TaskType.PLANNED)
        if (!taskTypePlanned) {
            taskTypePlanned = new TaskType(title: TaskType.PLANNED).save()
        }

        def taskTypeExtraAsked= TaskType.findByTitle(TaskType.EXTRA_ASKED)
        if (!taskTypeExtraAsked) {
            taskTypeExtraAsked = new TaskType(title: TaskType.EXTRA_ASKED).save()
        }

        def taskTypeExtraNotAsked= TaskType.findByTitle(TaskType.EXTRA_NOT_ASKED)
        if (!taskTypeExtraNotAsked) {
            taskTypeExtraNotAsked = new TaskType(title: TaskType.EXTRA_NOT_ASKED).save()
        }

        def taskTypeBugFix= TaskType.findByTitle(TaskType.BUG_FIX)
        if (!taskTypeBugFix) {
            taskTypeBugFix = new TaskType(title: TaskType.BUG_FIX).save()
        }

        println "Priority"


        def priorityVeryLow = Priority.findByNumber(Priority.VERY_LOW_PRIORITY_NUMBER)
        if (!priorityVeryLow) {
            priorityVeryLow = new Priority(title: Priority.VERY_LOW_PRIORITY,number: Priority.VERY_LOW_PRIORITY_NUMBER).save()
        }

        def priorityLow = Priority.findByNumber(Priority.LOW_PRIORITY_NUMBER)
        if (!priorityLow) {
            priorityLow = new Priority(title: Priority.LOW_PRIORITY,number: Priority.LOW_PRIORITY_NUMBER).save()
        }

        def priorityMedium = Priority.findByNumber(Priority.MEDIUM_PRIORITY_NUMBER)
        if (!priorityMedium) {
            priorityMedium = new Priority(title: Priority.MEDIUM_PRIORITY,number: Priority.MEDIUM_PRIORITY_NUMBER).save()
        }

        def priorityHigh = Priority.findByNumber(Priority.HIGH_PRIORITY_NUMBER)
        if (!priorityHigh) {
            priorityHigh = new Priority(title: Priority.HIGH_PRIORITY,number: Priority.HIGH_PRIORITY_NUMBER).save()
        }

        def priorityCritical = Priority.findByNumber(Priority.CRITICAL_PRIORITY_NUMBER)
        if (!priorityCritical) {
            priorityCritical = new Priority(title: Priority.CRITICAL_PRIORITY,number: Priority.CRITICAL_PRIORITY_NUMBER).save()
        }


        println "Task Status"

        def taskStatusNeedWork = TaskStatus.findByNumber(TaskStatus.NEED_WORK_ON_IT_NUMBER)
        if (!taskStatusNeedWork) {
            taskStatusNeedWork = new TaskStatus(title:TaskStatus.NEED_WORK_ON_IT,body:TaskStatus.NEED_WORK_ON_IT,
                    number: TaskStatus.NEED_WORK_ON_IT_NUMBER).save()
        }

        def taskStatusInWork = TaskStatus.findByNumber(TaskStatus.IN_WORK_NUMBER)
        if (!taskStatusInWork) {
            taskStatusInWork = new TaskStatus(title:TaskStatus.IN_WORK,body:TaskStatus.IN_WORK,
                    number: TaskStatus.IN_WORK_NUMBER).save()
        }

        def taskStatusComplete= TaskStatus.findByNumber(TaskStatus.COMPLETE_NUMBER)
        if (!taskStatusComplete) {
            taskStatusComplete = new TaskStatus(title:TaskStatus.COMPLETE,body:TaskStatus.COMPLETE,
                    number: TaskStatus.COMPLETE_NUMBER).save()
        }

        def taskStatusInWorkFrozen= TaskStatus.findByNumber(TaskStatus.IN_WORK_FROZEN_NUMBER)
        if (!taskStatusInWorkFrozen) {
            taskStatusInWorkFrozen = new TaskStatus(title:TaskStatus.IN_WORK_FROZEN,body:TaskStatus.IN_WORK_FROZEN,
                    number: TaskStatus.IN_WORK_FROZEN_NUMBER).save()
        }

        def taskStatusInWorkDropped= TaskStatus.findByNumber(TaskStatus.IN_WORK_DROPPED_NUMBER)
        if (!taskStatusInWorkDropped) {
            taskStatusInWorkDropped = new TaskStatus(title:TaskStatus.IN_WORK_DROPPED,body:TaskStatus.IN_WORK_DROPPED,
                    number: TaskStatus.IN_WORK_DROPPED_NUMBER).save()
        }


        def taskStatusFeatureNotWorking= TaskStatus.findByNumber(TaskStatus.FEATURE_NOT_WORK_ON_IT_NUMBER)
        if (!taskStatusFeatureNotWorking) {
            taskStatusFeatureNotWorking = new TaskStatus(title:TaskStatus.FEATURE_NOT_WORK_ON_IT,body:TaskStatus.FEATURE_NOT_WORK_ON_IT,
                    number: TaskStatus.FEATURE_NOT_WORK_ON_IT_NUMBER).save()
        }

        def taskStatusNotNeedWorkAnymore= TaskStatus.findByNumber(TaskStatus.NOT_NEED_ANYMORE_NUMBER)
        if (!taskStatusNotNeedWorkAnymore) {
            taskStatusNotNeedWorkAnymore = new TaskStatus(title:TaskStatus.NOT_NEED_ANYMORE,body:TaskStatus.NOT_NEED_ANYMORE,
                    number: TaskStatus.NOT_NEED_ANYMORE_NUMBER).save()
        }
        
        /*
          println "Operation on Project..."
        def projectOneTitle = "Тестовый проект номер 1"
         def projectOneTarget = "Тестирование системы на наличие ошибок"
         def projectOneBody =projectOneTitle+" и корректность работы "

         def projectOne = Project.findByUserAndTitle(user, projectOneTitle)

         if (projectOne == null) {
             println "Create new project..."
             projectOne = new Project(title: projectOneTitle,target: projectOneTarget,body: projectOneBody,user:user)
             projectOne.save()
             println "End creation project"
             println projectOne

         }


         println "Operation on Status for project 1..."
         def statusProjectOne = Status.findByProject(projectOne)

         if (statusProjectOne == null) {
             println "Create status for project..."
             statusProjectOne = new Status()
             println "Change state for status"
             println "StatusType Active is null: "+statusTypeActive==null?true:false
             statusProjectOne = Status.changeState(statusProjectOne,statusTypeActive)
             println "Set project for status"
             statusProjectOne.project = projectOne
             println "save status"
             statusProjectOne.save()

             projectOne.status = statusProjectOne
             projectOne.save()
         }*/




    }
    def destroy = {
    }
}
