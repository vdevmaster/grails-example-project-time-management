databaseChangeLog = {

	changeSet(author: "owner (generated)", id: "1435657258031-1") {
		createTable(tableName: "comment") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "commentPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "body", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "comment_type_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "target_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-2") {
		createTable(tableName: "comment_type") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "comment_typePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-3") {
		createTable(tableName: "priority") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "priorityPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "number", type: "integer") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-4") {
		createTable(tableName: "project") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "projectPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "body", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "end_date", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "start_date", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "status_id", type: "bigint")

			column(name: "target", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "bigint")
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-5") {
		createTable(tableName: "project_comment") {
			column(name: "project_comments_id", type: "bigint")

			column(name: "comment_id", type: "bigint")
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-6") {
		createTable(tableName: "role") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "rolePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "authority", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-7") {
		createTable(tableName: "status") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "statusPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "project_id", type: "bigint")

			column(name: "status_type_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "step_id", type: "bigint")
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-8") {
		createTable(tableName: "status_type") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "status_typePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-9") {
		createTable(tableName: "step") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "stepPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "body", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "end_date", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "order_number", type: "integer") {
				constraints(nullable: "false")
			}

			column(name: "project_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "start_date", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "status_id", type: "bigint")

			column(name: "title", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-10") {
		createTable(tableName: "step_comment") {
			column(name: "step_comments_id", type: "bigint")

			column(name: "comment_id", type: "bigint")
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-11") {
		createTable(tableName: "task") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "taskPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "body", type: "varchar(255)")

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "priority_id", type: "bigint")

			column(name: "project_id", type: "bigint")

			column(name: "step_id", type: "bigint")

			column(name: "target_time", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "task_status_id", type: "bigint")

			column(name: "task_type_id", type: "bigint")

			column(name: "title", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-12") {
		createTable(tableName: "task_comment") {
			column(name: "task_comments_id", type: "bigint")

			column(name: "comment_id", type: "bigint")
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-13") {
		createTable(tableName: "task_status") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "task_statusPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "body", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "number", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-14") {
		createTable(tableName: "task_time") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "task_timePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "end_time", type: "timestamp")

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "start_time", type: "timestamp")

			column(name: "task_id", type: "bigint")

			column(name: "time", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-15") {
		createTable(tableName: "task_type") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "task_typePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-16") {
		createTable(tableName: "user") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "userPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "account_expired", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "account_locked", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "enabled", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "password", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "password_expired", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "username", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-17") {
		createTable(tableName: "user_role") {
			column(name: "user_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "role_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-18") {
		addPrimaryKey(columnNames: "user_id, role_id", constraintName: "user_rolePK", tableName: "user_role")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-42") {
		createIndex(indexName: "authority_uniq_1435657257969", tableName: "role", unique: "true") {
			column(name: "authority")
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-43") {
		createIndex(indexName: "username_uniq_1435657257987", tableName: "user", unique: "true") {
			column(name: "username")
		}
	}

	changeSet(author: "owner (generated)", id: "1435657258031-19") {
		addForeignKeyConstraint(baseColumnNames: "comment_type_id", baseTableName: "comment", constraintName: "FK_lj0itq1x3c16ganupcet1uwb5", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "comment_type", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-20") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "comment", constraintName: "FK_mxoojfj9tmy8088avf57mpm02", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-21") {
		addForeignKeyConstraint(baseColumnNames: "status_id", baseTableName: "project", constraintName: "FK_cmq2womflwqgdijyxklbasgdi", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "status", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-22") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "project", constraintName: "FK_60d0g900v88hwu1mfng1nbewq", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-23") {
		addForeignKeyConstraint(baseColumnNames: "comment_id", baseTableName: "project_comment", constraintName: "FK_8enwp8nvtyelti6xx3w6ucshs", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "comment", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-24") {
		addForeignKeyConstraint(baseColumnNames: "project_comments_id", baseTableName: "project_comment", constraintName: "FK_a9slcryx4h0fykq0ujntkwnjo", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "project", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-25") {
		addForeignKeyConstraint(baseColumnNames: "project_id", baseTableName: "status", constraintName: "FK_1c9als66hvqv1hjlv4ohok4hl", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "project", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-26") {
		addForeignKeyConstraint(baseColumnNames: "status_type_id", baseTableName: "status", constraintName: "FK_ju6joy9cdjjo69a5atx2n2ou4", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "status_type", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-27") {
		addForeignKeyConstraint(baseColumnNames: "step_id", baseTableName: "status", constraintName: "FK_a2nywd831v58dhi1486sl51rm", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "step", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-28") {
		addForeignKeyConstraint(baseColumnNames: "project_id", baseTableName: "step", constraintName: "FK_ll26hu36i3amrn6t3w3ytlcp4", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "project", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-29") {
		addForeignKeyConstraint(baseColumnNames: "status_id", baseTableName: "step", constraintName: "FK_lsehblu71fuwfxq2iiqk1tsbr", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "status", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-30") {
		addForeignKeyConstraint(baseColumnNames: "comment_id", baseTableName: "step_comment", constraintName: "FK_d1f75ywvdj3rmqnhej5aqe26w", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "comment", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-31") {
		addForeignKeyConstraint(baseColumnNames: "step_comments_id", baseTableName: "step_comment", constraintName: "FK_eooxxl3p0r2vxh69frfal5e6v", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "step", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-32") {
		addForeignKeyConstraint(baseColumnNames: "priority_id", baseTableName: "task", constraintName: "FK_f1d40g6b7pmqefekbt4c8pc3r", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "priority", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-33") {
		addForeignKeyConstraint(baseColumnNames: "project_id", baseTableName: "task", constraintName: "FK_b7i81l1tk1ph95xnhtoftyv53", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "project", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-34") {
		addForeignKeyConstraint(baseColumnNames: "step_id", baseTableName: "task", constraintName: "FK_t6wsep1dx9x0g1ox52tgeuomw", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "step", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-35") {
		addForeignKeyConstraint(baseColumnNames: "task_status_id", baseTableName: "task", constraintName: "FK_2tk4gkv2qiiglolkgqoyicvf7", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "task_status", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-36") {
		addForeignKeyConstraint(baseColumnNames: "task_type_id", baseTableName: "task", constraintName: "FK_cqp03sw1hfv9q74jg2elitsbp", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "task_type", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-37") {
		addForeignKeyConstraint(baseColumnNames: "comment_id", baseTableName: "task_comment", constraintName: "FK_hlydm5y6b566bjwicocniitej", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "comment", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-38") {
		addForeignKeyConstraint(baseColumnNames: "task_comments_id", baseTableName: "task_comment", constraintName: "FK_316d2869q6shgovrl0jgttcmx", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "task", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-39") {
		addForeignKeyConstraint(baseColumnNames: "task_id", baseTableName: "task_time", constraintName: "FK_9k0bktkbgbd7w4rtqnslo1k3e", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "task", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-40") {
		addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "user_role", constraintName: "FK_it77eq964jhfqtu54081ebtio", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "role", referencesUniqueColumn: "false")
	}

	changeSet(author: "owner (generated)", id: "1435657258031-41") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_role", constraintName: "FK_apcc8lxk2xnug8377fatvbn04", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}
}
